/*
 * Class-level Member (Eager Initialization Method):
 * The singleton design pattern is used to restrict the instantiation of a class and ensures that 
 * only one instance of the class exists in the JVM. In other words, a singleton class is a class 
 * that can have only one object (an instance of the class) at a time per JVM instance.
 *  There are various ways to design/code a singleton class
 */
package com.org.shiavnski.java.tutorial;
public class SingletonTypeOne {
	private String introduction;

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	private static SingletonTypeOne SINGLE_INSTANCE = null;

	private SingletonTypeOne() {
	}

	public static SingletonTypeOne getInstance() {
		if (SINGLE_INSTANCE == null) {
			synchronized (SingletonTypeOne.class) {
				SINGLE_INSTANCE = new SingletonTypeOne();
			}
		}
		return SINGLE_INSTANCE;
	}

	public static void main(String[] args) {
		SingletonTypeOne singletonTypeOne = SingletonTypeOne.getInstance();
		singletonTypeOne.setIntroduction("instance one");
		System.out.println(singletonTypeOne.getIntroduction());
		SingletonTypeOne singletonTypetwo = SingletonTypeOne.getInstance();// same instance
		System.out.println(singletonTypetwo.getIntroduction());
	}

}
