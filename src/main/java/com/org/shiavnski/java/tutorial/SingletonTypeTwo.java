/*
 * Class-level Member (Lazy Initialization with double lock Method):
 * The singleton design pattern is used to restrict the instantiation of a class and ensures that 
 * only one instance of the class exists in the JVM. In other words, a singleton class is a class 
 * that can have only one object (an instance of the class) at a time per JVM instance.
 *  There are various ways to design/code a singleton class
 */

package com.org.shiavnski.java.tutorial;

public class SingletonTypeTwo {

	private String introduction;

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	private static SingletonTypeTwo SINGLE_INSTANCE = null;

	private SingletonTypeTwo() {
	}

	public static SingletonTypeTwo getInstance() {
		if (SINGLE_INSTANCE == null) {
			synchronized (SingletonTypeTwo.class) {
				if (SINGLE_INSTANCE == null) {
					SINGLE_INSTANCE = new SingletonTypeTwo();
				}
			}
		}
		return SINGLE_INSTANCE;
	}

	public static void main(String[] args) {
		SingletonTypeTwo SingletonTypeTwo1 = SingletonTypeTwo.getInstance();
		SingletonTypeTwo1.setIntroduction("instance one");
		System.out.println(SingletonTypeTwo1.getIntroduction());
		SingletonTypeTwo singletonTypeTwo2 = SingletonTypeTwo.getInstance();// same instance
		System.out.println(singletonTypeTwo2.getIntroduction());
	}
}
